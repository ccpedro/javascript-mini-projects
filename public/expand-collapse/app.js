const body = document.querySelector('body');

body.addEventListener('click', e => {
    //If the target is not the expand button, return and ends the evnt listener
    if(!e.target.matches('.expand-button')) return
    
    //Verify the button text and toggles to Expand or Collapse
    e.target.innerText = e.target.innerText.includes('Expand') ? 'Collapse': 'Expand';
    const card = e.target.closest('.card');
    const cardBody = card.querySelector('.card-body');
    cardBody.classList.toggle('show'); // toogles the class 'show' in the card-body div
    
});