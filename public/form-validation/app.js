//Select all elements needed
//Use the HTML to figure out what classes/ids will work best for selecting each element
const form = document.querySelector('#form');
const userName = document.querySelector('#username');
const password = document.querySelector('#password');
const passwordConfirm = document.querySelector('#password-confirmation');
const terms = document.querySelector('#terms');
const errorsList = document.querySelector('.errors-list');
const errors = document.querySelector('.errors');
console.log(form);
console.log(errors);


//Create an event listener for when the form is submitted and do the following inside of it.
form.addEventListener('submit', (e) => {
    //Create an array to store all error messages and clear any old error messages
    const errorsMsg = [];
    clearErrors();
    console.log(userName.value, password.value, passwordConfirm.value)
    //    TODO: Define the following validation checks with appropriate error messages
    //      1. Ensure the username is at least 6 characters long
    if(userName.value.length < 6) errorsMsg.push("Ensure the username is at least 6 characters long");
    //      2. Ensure the password is at least 10 characters long
    if(password.value.length < 10) errorsMsg.push("Ensure the password is at least 10 characters long");
    //      3. Ensure the password and confirmation password match
    if(password.value !== passwordConfirm.value) errorsMsg.push("Ensure the password and confirmation password match");
    if(!terms.checked) errorsMsg.push("Ensure the terms is checked");
    if(errorsMsg.length > 0) {
      e.preventDefault();
      showErrors(errorsMsg);
      console.log(Array.from(errorsList.children))
    }
    

});




//      4. Ensure the terms checkbox is checked
//    TODO: If there are any errors then prevent the form from submitting and show the error messages

// TODO: Define this function
function clearErrors() {
    // Loop through all the children of the error-list element and remove them
    // IMPORTANT: This cannot be done with a forEach loop or a normal for loop since as you remove children it will modify the list you are looping over which will not work
    // I recommend using a while loop to accomplish this task
    // This is the trickiest part of this exercise so if you get stuck and are unable to progress you can also set the innerHTML property of
    // the error-list to an empty string and that will also clear the children. I recommend trying to accomplish this with a while loop, though, for practice.
    errorsList.innerHTML = '';
    // Also, make sure you remove the show class to the errors container
    errors.classList.remove("show");
  }
  
  // TODO: Define this function
  function showErrors(errorMessages) {
    // Add each error to the error-list element
    errorMessages.forEach(msg => {
        errorsList.innerHTML += `<li>${msg}</li>`
    });
    // Make sure to use an li as the element for each error
    // Also, make sure you add the show class to the errors container
    errors.classList.add('show');
  }
  